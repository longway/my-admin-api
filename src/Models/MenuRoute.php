<?php

namespace ShandiaLamp\MyAdmin\Models;

use Illuminate\Database\Eloquent\Model;

class MenuRoute extends Model
{
    protected $table = 'menu_route';
    
    protected $fillable = [
        'menu_id',
        'route'
    ];

    public function menus()
    {
        return $this->hasMany(Menu::class, 'menu_id');
    }
}
