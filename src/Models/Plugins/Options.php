<?php

namespace ShandiaLamp\MyAdmin\Models\Plugins;

use Illuminate\Database\Eloquent\Builder;

trait Options
{
    public function scopeToOptions(Builder $query, $label)
    {
        return $query->get()->map(function ($item) use ($label) {
            return [
                'value' => $item->id,
                'label' => $item->$label
            ];
        });
    }
}
