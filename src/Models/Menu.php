<?php

namespace ShandiaLamp\MyAdmin\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $fillable = [
        'name',
        'type',
        'url',
        'icon',
        'parent_id'
    ];

    public function routes()
    {
    
        return $this->hasMany(MenuRoute::class, 'menu_id');
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_menu');
    }

    public function menus()
    {
        return $this->hasMany(Menu::class, 'parent_id');
    }

    public function scopeRoot(Builder $query)
    {
        return $query->where('parent_id', null);
    }

    public function scopeOfParent(Builder $query, $parentID)
    {
        return $query->where('parent_id', $parentID);
    }

    public function scopeToTree()
    {
        function render($menus)
        {
            return $menus->map(function ($item) {
                $item->children = render(Menu::ofParent($item->id)->get());
                return [
                    'value'     => $item->id,
                    'label'     => $item->name,
                    'children'  => $item->children
                ];
            });
        }
        return render($this->root()->get());
    }
}
