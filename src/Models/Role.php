<?php

namespace ShandiaLamp\MyAdmin\Models;

use Illuminate\Database\Eloquent\Model;
use ShandiaLamp\MyAdmin\Models\Plugins\Options;

class Role extends Model
{
    use Options;

    protected $fillable = [
        'role'
    ];

    public function staff()
    {
        return $this->belongsToMany(Staff::class, 'role_staff');
    }

    public function menus()
    {
        return $this->belongsToMany(Menu::class, 'role_menu');
    }
}
