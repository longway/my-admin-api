<?php

namespace ShandiaLamp\MyAdmin\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Staff extends User implements JWTSubject
{
    protected $fillable = [
        'name', 'email', 'password'
    ];

    protected $hidden = [
        'password',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_staff');
    }

    public function scopeIsAdmin()
    {
        return $this->roles()->where('deletable', 0)->first() ? true : false;
    }
}
