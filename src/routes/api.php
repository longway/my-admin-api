<?php


$params = env('APP_ENV') === 'testing' ? [] : ['middleware' => 'auth.jwt'];

Route::post('/login', 'AuthController@login');

Route::group($params, function () {
    Route::get('/me', 'MeController@index');
    Route::get('/logout', 'AuthController@logout');

    Route::get('/menus', 'MenuController@index')->name('menu.query');
    Route::post('/menus', 'MenuController@store')->name('menu.store');
    Route::delete('/menus/{id}', 'MenuController@destroy')->name('menu.destroy');

    Route::get('/staff', 'StaffController@index')->name('staff.query');
    Route::get('/staff/{id}', 'StaffController@show')->name('staff.show');
    Route::post('/staff', 'StaffController@store')->name('staff.store');
    Route::put('/staff/{id}', 'StaffController@update')->name('staff.update');
    Route::delete('/staff/{id}', 'StaffController@destory')->name('staff.destroy');

    Route::get('/roles', 'RoleController@index')->name('role.query');
    Route::get('/roles/{id}', 'RoleController@show')->name('role.show');
    Route::post('/roles', 'RoleController@store')->name('role.store');
    Route::put('/roles/{id}', 'RoleController@update')->name('role.update');
    Route::delete('/roles/{id}', 'RoleController@destory')->name('role.destroy');
    
});
Route::get('/permissions', 'PermissionController@index')->name('permission.query');
