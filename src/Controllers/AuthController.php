<?php

namespace ShandiaLamp\MyAdmin\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use ShandiaLamp\MyAdmin\Models\Menu;
use ShandiaLamp\MyAdmin\Models\Staff;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $input = $request->only('email', 'password');

        if (!$token = JWTAuth::attempt($input)) {
            return response()->json([
                'success' => false,
                'message' => '用户名或密码错误',
            ], 401);
        }

        $user = Staff::where('email', $input['email'])->first();
        $redirect = null;
        if ($user->isAdmin()) {
            $redirect = Menu::where('type', 'route')->first()->url;
        } else {
            $redirect = $user->roles[0]->menus()->where('type', 'route')->first()->url;
        }
        return response()->json([
            'token' => $token,
            'redirect' => $redirect
        ]);
    }

    public function logout(Request $request)
    {
        try {
            JWTAuth::invalidate($request->token);

            return response()->json([]);
        } catch (JWTException $exception) {
            return response('', 500);
        }
    }
}
