<?php

namespace ShandiaLamp\MyAdmin\Controllers;

use Illuminate\Routing\Controller;
use Auth;

class MeController extends Controller
{
    public function index()
    {
        return response()->json([
            'data' => Auth::user()
        ]);
    }
}
