<?php

namespace ShandiaLamp\MyAdmin\Controllers;

use Illuminate\Routing\Controller;
use ShandiaLamp\MyAdmin\Models\Menu;
use ShandiaLamp\MyAdmin\Requests\Menu\StoreRequest;
use ShandiaLamp\MyAdmin\Models\MenuRoute;
use ShandiaLamp\MyAdmin\Requests\Menu\IndexRequest;
use ShandiaLamp\MyAdmin\Requests\Menu\DestroyRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Auth;
use DB;
use Exception;

class MenuController extends Controller
{
    public function index(IndexRequest $request)
    {
        unset($request);
        return response()->json([
            'data' => $this->render(Menu::root()->get()),
        ]);
    }

    protected function render($menus)
    {
        return $menus->map(function ($item) {
            $item->children = $this->render(Menu::ofParent($item->id)->get());
            return $item;
        })->filter(function ($item) {
            $roleIDs = Auth::user()->roles()->pluck('id')->toArray();
            return $item->roles()->whereIn('id', $roleIDs)->count() > 0
                    || $item->children->count() > 0
                    || Auth::user()->isAdmin();
        })->values();
    }

    public function store(StoreRequest $request)
    {
        DB::beginTransaction();
        try {
            $menu = Menu::create($request->only([
                'parent_id',
                'name',
                'url',
                'type',
                'icon'
            ]));
            
            MenuRoute::insert(collect($request->get('routes', []))->map(function ($item) use ($menu) {
                return [
                    'menu_id' => $menu->id,
                    'route' => $item
                ];
            })->toArray());
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            throw new HttpException(500);
        }

        return response()->json([
            'data' => $menu
        ]);
    }

    public function destroy($id, DestroyRequest $request)
    {
        unset($request);
        $menu = Menu::findOrFail($id);

        $menu->menus->each(function ($item) {
            $item->roles()->detach();
            $item->routes()->delete();
            $item->delete();
        });
        $menu->roles()->detach();
        $menu->routes()->delete();
        $menu->delete();

        return response()->noContent();
        return response()->noContent();
    }
}
