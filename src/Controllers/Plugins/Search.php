<?php

namespace ShandiaLamp\MyAdmin\Controllers\Plugins;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use ShandiaLamp\MyAdmin\Controllers\Plugins\Search\Between;
use ShandiaLamp\MyAdmin\Controllers\Plugins\Search\Equal;
use ShandiaLamp\MyAdmin\Controllers\Plugins\Search\Like;

trait Search
{
    public function search(Request $request, Model $model) : Builder
    {
        $search = $request->get('search');
        $search = explode(';', $search);
        $query = $model->query();
        foreach ($search as $item) {
            $query = $this->findItem($query, $item);
        }
        return $query;
    }

    private function findItem(Builder $query, $str)
    {
        $arr = explode(':', $str);
        $count = count($arr);
        if ($count > 1) {
            $key = $arr[0];
            $value = $arr[1];
            $operation = $count > 2 ? $arr[2] : '=';
            $handler = $this->operation($operation);
            $query = $handler->handle($query, $key, $value);
        }
        return $query;
    }

    private function operation($operation)
    {
        $supports = [
            Equal::class => '=',
            Like::class  => 'like',
            Between::class => 'between'
        ];

        foreach ($supports as $key => $value) {
            if ($value === $operation) {
                return new $key();
            }
        }
        return new Equal();
    }
}
