<?php

namespace ShandiaLamp\MyAdmin\Controllers\Plugins;

use Closure;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Pagination\LengthAwarePaginator;

trait Paginate
{
    public $pageSize = 10;

    public function paginate(Builder $query, Closure $transform = null, array $extend = [])
    {
        $result = $query->paginate($this->pageSize);
        $items = $result->items();
        if ($transform) {
            $items = collect($items)->map($transform);
        }
        return response()->json(
            array_merge([
                'data' => $items,
                'meta' => [
                    'pagination' => [
                        'total' => $result->total(),
                        'count' => count($items),
                        'current_page' => $result->currentPage(),
                        'per_page' => $result->perPage()
                    ]
                ]
            ], $extend)
        );
    }
}
