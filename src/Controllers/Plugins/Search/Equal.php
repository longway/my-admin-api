<?php

namespace ShandiaLamp\MyAdmin\Controllers\Plugins\Search;

use Illuminate\Database\Eloquent\Builder;

class Equal implements SearchInterface
{
    public function handle(Builder $query, $key, $value)
    {
        $keys = explode(".", $key);
        if (count($keys) > 1) {
            $key = $keys[1];
            $relation = $keys[0];
            return $query->whereHas($relation, function ($query) use ($key, $value) {
                return $query->where($key, $value);
            });
        }
        return $query->where($key, $value);
    }
}
