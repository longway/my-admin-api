<?php


namespace ShandiaLamp\MyAdmin\Controllers\Plugins\Search;

use Illuminate\Database\Eloquent\Builder;

interface SearchInterface
{
    public function handle(Builder $query, $key, $value);
}
