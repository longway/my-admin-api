<?php

namespace ShandiaLamp\MyAdmin\Controllers\Plugins\Search;

use Illuminate\Database\Eloquent\Builder;

class Between implements SearchInterface
{
    public function handle(Builder $query, $key, $value)
    {
        return $query->whereBetween($key, explode(',', $value));
    }
}
