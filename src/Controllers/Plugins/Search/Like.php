<?php

namespace ShandiaLamp\MyAdmin\Controllers\Plugins\Search;

use Illuminate\Database\Eloquent\Builder;

class Like implements SearchInterface
{
    public function handle(Builder $query, $key, $value)
    {
        return $query->where($key, 'like', "%$value%");
    }
}
