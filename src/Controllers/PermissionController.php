<?php

namespace ShandiaLamp\MyAdmin\Controllers;

use Illuminate\Routing\Controller;

class PermissionController extends Controller
{
    public function index()
    {
        $routes = app('router')->getRoutes()->getRoutes();
        $data = [];
        foreach ($routes as $route) {
            $name = $route->getName();
            if ($name) {
                $data[] = [
                    'key' => $name,
                    'api_url' => $route->uri(),
                    'api_method' => $route->methods()[0]
                ];
            }
        }
        return response()->json([
            'data' => $data
        ]);
    }
}
