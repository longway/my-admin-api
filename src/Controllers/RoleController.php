<?php

namespace ShandiaLamp\MyAdmin\Controllers;

use App\Http\Controllers\Controller;
use ShandiaLamp\MyAdmin\Controllers\Plugins\Paginate;
use ShandiaLamp\MyAdmin\Controllers\Plugins\Search;
use ShandiaLamp\MyAdmin\Models\Menu;
use ShandiaLamp\MyAdmin\Models\Role;
use ShandiaLamp\MyAdmin\Requests\Role\DestroyRequest;
use ShandiaLamp\MyAdmin\Requests\Role\IndexRequest;
use ShandiaLamp\MyAdmin\Requests\Role\ShowRequest as RoleShowRequest;
use ShandiaLamp\MyAdmin\Requests\Role\StoreRequest;
use ShandiaLamp\MyAdmin\Requests\Role\UpdateRequest;
use ShandiaLamp\MyAdmin\Requests\Staff\ShowRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;

class RoleController extends Controller
{
    use Paginate;
    use Search;

    public function index(IndexRequest $request)
    {
        $query = $this->search($request, new Role());
        return $this->paginate($query, null, [
            'menus' => Menu::toTree()
        ]);
    }

    public function show($id, ShowRequest $request)
    {
        unset($request);
        $role = Role::findOrFail($id);
        $menus = $role->menus->map(function ($role) {
            return $role->id;
        });
        unset($role->menus);
        $role->menus = $menus;
        return response()->json([
            'data' => $role
        ]);
    }

    public function store(StoreRequest $request)
    {
        $role = Role::create($request->only([
            'role',
        ]));
        $role->menus()->attach(collect($request->get('menus'))->map(function ($item) {
            return [
                'menu_id' => $item
            ];
        })->toArray());


        return response()->json([
            'data' => $role
        ]);
    }

    public function update($id, UpdateRequest $request)
    {
        $role = Role::findOrFail($id);

        
        $role->menus()->detach();
        $role->menus()->attach(
            collect($request->get('menus', []))->map(function ($item) {
                return [
                    'menu_id' => $item
                ];
            })->toArray()
        );

        return response()->json([
            'data' => $role
        ]);
    }

    public function destory($id, DestroyRequest $request)
    {
        unset($request);
        $role = Role::findOrFail($id);

        if (!$role->deletable) {
            throw new HttpException(400, '该角色无法被删除');
        }
        $role->staff()->detach();
        $role->delete();

        return response()->noContent();
    }
}
