<?php

namespace ShandiaLamp\MyAdmin\Controllers;

use App\Http\Controllers\Controller;
use Exception;
use ShandiaLamp\MyAdmin\Controllers\Plugins\Paginate;
use ShandiaLamp\MyAdmin\Controllers\Plugins\Search;
use ShandiaLamp\MyAdmin\Models\Role;
use ShandiaLamp\MyAdmin\Models\Staff;
use ShandiaLamp\MyAdmin\Requests\Staff\StoreRequest;
use DB;
use ShandiaLamp\MyAdmin\Requests\Staff\IndexRequest;
use ShandiaLamp\MyAdmin\Requests\Staff\ShowRequest;
use ShandiaLamp\MyAdmin\Requests\Staff\UpdateRequest;
use ShandiaLamp\MyAdmin\Requests\Staff\DestroyRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;

class StaffController extends Controller
{
    use Paginate;
    use Search;

    public function index(IndexRequest $request)
    {
        $query = $this->search($request, new Staff());
        return $this->paginate($query, function ($item) {
            $item->role = $item->roles->map(function ($role) {
                return $role->role;
            })->join(',');
            return $item;
        }, [
            'roles' => Role::toOptions('role')
        ]);
    }

    public function show($id, ShowRequest $request)
    {
        unset($request);
        $staff = Staff::findOrFail($id);
        $roles = $staff->roles->map(function ($role) {
            return $role->id;
        });
        unset($staff->roles);
        $staff->roles = $roles;
        return response()->json([
            'data' => $staff
        ]);
    }

    public function store(StoreRequest $request)
    {
        $data = $request->only([
            'name',
            'email'
        ]);
        $data['password'] = bcrypt('123456');
        try {
            DB::beginTransaction();
            $staff = Staff::create($data);
            $staff->roles()->attach(
                collect($request->get('roles', []))->map(function ($item) {
                    return [
                        'role_id' => $item
                    ];
                })->toArray()
            );
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            throw new HttpException(500);
        }

        return response()->json([
            'data' => $staff
        ]);
    }

    public function update($id, UpdateRequest $request)
    {
        $staff = Staff::findOrFail($id);

        
        $staff->roles()->detach();
        $staff->roles()->attach(
            collect($request->get('roles', []))->map(function ($item) {
                return [
                    'role_id' => $item
                ];
            })->toArray()
        );

        return response()->json([
            'data' => $staff
        ]);
    }

    public function destory($id, DestroyRequest $request)
    {
        unset($request);
        $staff = Staff::findOrFail($id);

        $staff->roles()->detach();
        $staff->delete();

        return response()->noContent();
    }
}
