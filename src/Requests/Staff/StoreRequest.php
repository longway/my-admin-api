<?php

namespace ShandiaLamp\MyAdmin\Requests\Staff;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use ShandiaLamp\MyAdmin\Requests\Authorize;

class StoreRequest extends FormRequest
{
    use Authorize;

    public function authorize()
    {
        return $this->verify();
    }

    public function rules()
    {
        return [
            'name' => 'required',
            'email' => [
                'required',
                Rule::unique('staff')
            ]
        ];
    }

    public function messages()
    {
        return [
            'name.required' => '姓名不能为空',
            'email.required' => '邮箱不能为空',
            'email.unique' => '邮箱已被使用'
        ];
    }
}
