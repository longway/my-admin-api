<?php

namespace ShandiaLamp\MyAdmin\Requests;

use Auth;

trait Authorize
{
    public function verify()
    {
        if (env('APP_ENV') === 'testing') {
            return true;
        }
        if (Auth::user()->isAdmin()) {
            return true;
        }
        $name = request()->route()->getName();
        foreach (Auth::user()->roles as $role) {
            foreach ($role->menus as $menu) {
                foreach ($menu->routes as $route) {
                    if ($route->route == $name) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
