<?php

namespace ShandiaLamp\MyAdmin\Requests\Menu;

use Illuminate\Foundation\Http\FormRequest;
use ShandiaLamp\MyAdmin\Requests\Authorize;

class StoreRequest extends FormRequest
{
    use Authorize;

    public function authorize()
    {
        return $this->verify();
    }

    public function rules()
    {
        return [
            'name' => 'required',
            'type' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => '名称不能为空',
            'type.required' => '类型不能为空',
        ];
    }
}
