<?php

namespace ShandiaLamp\MyAdmin\Requests\Menu;

use Illuminate\Foundation\Http\FormRequest;
use ShandiaLamp\MyAdmin\Requests\Authorize;

class IndexRequest extends FormRequest
{
    use Authorize;

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
        ];
    }

    public function messages()
    {
        return [
        ];
    }
}
