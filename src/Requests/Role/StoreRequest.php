<?php

namespace ShandiaLamp\MyAdmin\Requests\Role;

use Illuminate\Foundation\Http\FormRequest;
use ShandiaLamp\MyAdmin\Requests\Authorize;

class StoreRequest extends FormRequest
{
    use Authorize;

    public function authorize()
    {
        return $this->verify();
    }

    public function rules()
    {
        return [
            'role' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'role.required' => '角色不能为空'
        ];
    }
}
