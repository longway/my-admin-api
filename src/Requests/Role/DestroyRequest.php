<?php

namespace ShandiaLamp\MyAdmin\Requests\Role;

use Illuminate\Foundation\Http\FormRequest;
use ShandiaLamp\MyAdmin\Requests\Authorize;

class DestroyRequest extends FormRequest
{
    use Authorize;

    public function authorize()
    {
        return $this->verify();
    }

    public function rules()
    {
        return [];
    }
}
