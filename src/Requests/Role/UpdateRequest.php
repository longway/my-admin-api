<?php

namespace ShandiaLamp\MyAdmin\Requests\Role;

use Illuminate\Foundation\Http\FormRequest;
use ShandiaLamp\MyAdmin\Requests\Authorize;

class UpdateRequest extends FormRequest
{
    use Authorize;

    public function authorize()
    {
        return $this->verify();
    }

    public function rules()
    {
        return [
        ];
    }
}
