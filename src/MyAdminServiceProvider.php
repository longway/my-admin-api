<?php

namespace ShandiaLamp\MyAdmin;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class MyAdminServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerRoute();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../database/migrations' => database_path('migrations'),
            __DIR__ . '/../database/seeds' => database_path('seeds'),
        ], 'my-admin');
    }

    protected function registerRoute()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace('\ShandiaLamp\MyAdmin\Controllers')
             ->group(__DIR__ . '/routes/api.php');
    }
}
