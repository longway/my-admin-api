# My Admin API

## 安装

```
composer require shandialamp/my-admin-api
```

## 代码规范

```
composer lint
```

## 测试

```
composer test
```