<?php

use Illuminate\Database\Seeder;
use ShandiaLamp\MyAdmin\Models\Menu;
use ShandiaLamp\MyAdmin\Models\Role;
use ShandiaLamp\MyAdmin\Models\Staff;

class BasicsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Menu::count()) {
            return false;
        }

        $base = Menu::create([
            'name'  => '基础管理',
            'type'  => 'group',
        ]);
        $menus = [
            [
                'name'  => '人员管理',
                'type'  => 'route',
                'url'   => '/staff',
                'parent_id' => $base->id
            ],
            [
                'name'  => '添加人员',
                'type'  => 'hidden',
                'url'   => '/staff/create',
                'parent_id' => $base->id
            ],
            [
                'name'  => '修改人员',
                'type'  => 'hidden',
                'url'   => '/staff/update',
                'parent_id' => $base->id
            ],
            [
                'name'  => '角色管理',
                'type'  => 'route',
                'url'   => '/roles',
                'parent_id' => $base->id
            ],
            [
                'name'  => '添加角色',
                'type'  => 'hidden',
                'url'   => '/roles/create',
                'parent_id' => $base->id
            ],
            [
                'name'  => '修改角色',
                'type'  => 'hidden',
                'url'   => '/roles/update',
                'parent_id' => $base->id
            ],
            [
                'name'  => '菜单管理',
                'type'  => 'route',
                'url'   => '/menus',
                'parent_id' => $base->id
            ]
        ];
        foreach ($menus as $menu) {
            Menu::create($menu);
        }

        $role = Role::create([
            'role' => '管理员'
        ]);
        $staff = Staff::create([
            'name'  => '管理员',
            'email' => 'admin@admin.com',
            'password' => bcrypt('123456')
        ]);
        $staff->roles()->attach([
            [
                'role_id' => $role->id
            ]
        ]);
    }
}
