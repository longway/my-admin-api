<?php

namespace ShandiaLamp\MyAdmin\Tests;

use Illuminate\Foundation\Testing\RefreshDatabase;
use ShandiaLamp\MyAdmin\Models\Staff;

class StaffTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        Staff::create([
            'name' => '林康贵',
            'email' => 'old@dd.com',
            'password' => '123456'
        ]);
    }

    /**
     * 测试获取成员列表
     */
    public function testQueryStaff()
    {
        $response = $this->get('/api/staff');

        $response->assertStatus(200);
    }

    /**
     * 测试获取指定ID成员信息
     */
    public function testShowStaff()
    {
        $staff = Staff::create([
            'name' => '林康贵',
            'email' => 'show@dd.com',
            'password' => '123456'
        ]);
        $response = $this->get("/api/staff/{$staff->id}");

        $response->assertStatus(200);
    }

    /**
     * 测试获取不存在成员信息
     */
    public function testShowStaffNotFound()
    {
        $response = $this->get('/api/staff/10000000');

        $response->assertStatus(404);
    }

    /**
     * 测试添加成员
     */
    public function testCreateStaff()
    {
        $response = $this->postJson('/api/staff', [
            'name' => '林康贵',
            'email' => 'lkg@dd.com'
        ]);

        $response->assertStatus(200);
    }

    /**
     * 测试添加成员验证规则
     * @dataProvider validateCreateStaffData
     */
    public function testValidateCreateStaff($name, $email)
    {
        $response = $this->postJson('/api/staff', [
            'name' => $name,
            'email' => $email
        ]);

        $response->assertStatus(422);
    }

    public function validateCreateStaffData()
    {
        return [
            ['', 'new@dd.com'],
            ['林康贵', ''],
            ['林康贵', 'old@dd.com']
        ];
    }

    /**
     * 测试删除成员
     */
    public function testDestoryStaff()
    {
        $staff = Staff::create([
            'name' => '林康贵',
            'email' => 'destory@dd.com',
            'password' => '123456'
        ]);
        $response = $this->deleteJson("/api/staff/{$staff->id}");

        $response->assertStatus(204);
    }

    /**
     * 测试删除不存在成员
     */
    public function testDestoryStaffNotFound()
    {
        $response = $this->deleteJson("/api/staff/10000000");

        $response->assertStatus(404);
    }
}
