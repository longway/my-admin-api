<?php

namespace ShandiaLamp\MyAdmin\Tests;

use Illuminate\Contracts\Console\Kernel;
use Illuminate\Filesystem\Filesystem;
use ShandiaLamp\MyAdmin\MyAdminServiceProvider;
use Tymon\JWTAuth\Providers\LaravelServiceProvider;

trait CreatesApplication
{
    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__ . '/../vendor/laravel/laravel/bootstrap/app.php';

        $app->make('Illuminate\Contracts\Console\Kernel')->bootstrap();
        $app->register(MyAdminServiceProvider::class);
        $app->register(LaravelServiceProvider::class);
        $app['router']->aliasMiddleware('auth.jwt', \Tymon\JWTAuth\Http\Middleware\Authenticate::class);

        $config = require __DIR__ . '/../config.php';

        $app['config']->set('database.default', 'mysql');
        $app['config']->set('database.connections.mysql.host', $config['host']);
        $app['config']->set('database.connections.mysql.port', $config['port']);
        $app['config']->set('database.connections.mysql.database', $config['database']);
        $app['config']->set('database.connections.mysql.username', $config['username']);
        $app['config']->set('database.connections.mysql.password', $config['password']);
        $app['config']->set('app.key', 'WTJ4nJFCwMIIvnq3Dh+3kPkiJVJR8I9/kHzp4aiK8rs=');

        require __DIR__ . '/../src/routes/api.php';

        return $app;
    }

    protected function migrateTestTables()
    {
        
        $fileSystem = new Filesystem();
        $fileSystem->requireOnce(__DIR__ . '/../database/migrations/2020_04_01_033514_create_staff_table.php');
        $fileSystem->requireOnce(__DIR__ . '/../database/migrations/2020_04_03_080243_create_menus_table.php');
        $fileSystem->requireOnce(__DIR__ . '/../database/migrations/2020_04_04_121020_create_roles_table.php');
        $fileSystem->requireOnce(__DIR__ . '/../database/migrations/2020_04_04_131957_create_role_staff_table.php');
        $fileSystem->requireOnce(__DIR__ . '/../database/migrations/2020_04_07_074355_create_role_menu_table.php');
        $fileSystem->requireOnce(__DIR__ . '/../database/migrations/2020_04_21_063919_create_menu_route_table.php');

        (new \CreateStaffTable())->up();
        (new \CreateMenusTable())->up();
        (new \CreateRolesTable())->up();
        (new \CreateRoleStaffTable())->up();
        (new \CreateRoleMenuTable())->up();
        (new \CreateMenuRouteTable())->up();
    }

    protected function destroyTestTables()
    {
        (new \CreateStaffTable())->down();
        (new \CreateMenusTable())->down();
        (new \CreateRolesTable())->down();
        (new \CreateRoleStaffTable())->down();
        (new \CreateRoleMenuTable())->down();
        (new \CreateMenuRouteTable())->down();
    }
}
