<?php

namespace ShandiaLamp\MyAdmin\Tests;

use Illuminate\Foundation\Testing\RefreshDatabase;
use ShandiaLamp\MyAdmin\Models\Role;

class RoleTest extends TestCase
{
    use RefreshDatabase;

    /**
     * 测试获取角色列表
     */
    public function testQueryRole()
    {
        $response = $this->get('/api/roles');

        $response->assertStatus(200);
    }

    /**
     * 测试获取指定ID角色信息
     */
    public function testShowRole()
    {
        $role = Role::create([
            'role' => '测试'
        ]);
        $response = $this->get("/api/roles/{$role->id}");
        
        $response->assertStatus(200);
    }

    /**
     * 测试获取不存在角色
     */
    public function testShowRoleNotFound()
    {
        $response = $this->get('/api/roles/10000000');

        $response->assertStatus(404);
    }

    /**
     * 测试添加角色
     */
    public function testCreateRole()
    {
        $response = $this->postJson('/api/roles', [
            'role' => '测试',
        ]);

        $response->assertStatus(200);
    }

    /**
     * 测试删除角色
     */
    public function testDestoryRole()
    {
        $staff = Role::create([
            'role' => '测试',
        ]);
        $response = $this->deleteJson("/api/roles/{$staff->id}");

        $response->assertStatus(204);
    }

    /**
     * 测试删除不存在角色
     */
    public function testDestoryRoleNotFound()
    {
        $response = $this->deleteJson("/api/roles/10000000");

        $response->assertStatus(404);
    }
}
