<?php

namespace ShandiaLamp\MyAdmin\Tests;

use Illuminate\Foundation\Testing\RefreshDatabase;
use ShandiaLamp\MyAdmin\Models\Menu;

class MenuTest extends TestCase
{
    use RefreshDatabase;

    /**
     * 测试获取菜单
     */
    public function testQueryMenu()
    {
        $response = $this->get('/api/menus');

        $response->assertStatus(200);
    }

    /**
     * 测试添加菜单
     */
    public function testCreateMenu()
    {
        $response = $this->postJson('/api/menus', [
            'name'  => '测试',
            'url'   => '/test',
            'type'  => 'route',
        ]);

        $response->assertStatus(200);
    }

    /**
     * 测试删除菜单
     */
    public function testDestoryMenu()
    {
        $staff = Menu::create([
            'name'  => '测试',
            'url'   => '/test',
            'type'  => 'route',
        ]);
        $response = $this->deleteJson("/api/menus/{$staff->id}");

        $response->assertStatus(204);
    }

    /**
     * 测试删除不存在菜单
     */
    public function testDestoryMenuNotFound()
    {
        $response = $this->deleteJson("/api/menus/10000000");

        $response->assertStatus(404);
    }
}
