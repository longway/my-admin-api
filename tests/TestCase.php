<?php

namespace ShandiaLamp\MyAdmin\Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected function setUp(): void
    {
        parent::setUp();
        $this->migrateTestTables();
    }

    protected function tearDown() : void
    {
        $this->destroyTestTables();
        parent::tearDown();
    }
}
